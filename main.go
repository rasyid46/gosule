package main

import (
	_ "bapi/routers"

	"github.com/astaxie/beego"
)

func main() {

	beego.BConfig.WebConfig.DirectoryIndex = true
	beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"

	beego.Run()
}
